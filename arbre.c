#include "arbre.h"

/*******************************************************************************
 Parcours l'arbre d'adresse racine 'r' en fonction du 'mot' dans le but de
 verifier jusqu'a quelle lettre le mot est present dans l'arbre. Pour ce faire,
 on effectue la recherche et on renvoie:
    - un prec qui pointe sur l'adresse de lien vertical precedant la valeur lue
    - l'index de la derniere lettre lue dans le mot

 @entree r:     Adresse de la racine de l'arbre qui est un pointeur de noeud
 @entree mot:   Adresse de la case actuelle du mot de la recherche

 @sortie index: Adresse de retour pour stocker le nombre de lettres trouvees
 @return        Double pointeur qui contiendra l'adresse du lien vertical ou
                horizontal du noeud precedant l'emplacement theorique (dans
                l'arbre) de la derniere lettre du mot lue.
*******************************************************************************/
arbre_t ** recherche(arbre_t ** r, val_t * mot[])
{
  int         STOP  = 0;  /* Booleen qui passe a VRAI quand on a fini la recherche */
  arbre_t **  prec  = r;  /* Prec pointe d'abord sur l'adresse de la racine de l'arbre */

  /* printf("debut: %p\n", prec); */

  /* Parcours vertical des lettres deja presentes */
  while (!STOP && **mot)
  {
    /* On realise les comparaisons de valeur en les mettant en minuscule */
    /* Parcours horizontal des fils du noeud */
    while(*prec && tolower((*prec)->valeur) < tolower(**mot) )
    {
      /*printf("Val noeud: %c\n", tolower((*prec)->valeur));*/
      prec = &((*prec)->frere);     /* On passe au frere via le lien horizontal */
    }
    /*printf("Lettre: %c\n", **mot);*/

    /* Si la valeur en sortie de boucle correspond a la lettre */
    if(*prec && tolower((*prec)->valeur) == tolower(**mot) )
    {
      prec  = &((*prec)->fils); /* On descend d'un niveau dans l'arbre */
      /*printf("%c -- ", **mot);*/
      (*mot)++;                 /* On incremente l'index du mot en cours */
      /*printf("%c\n", **mot);*/
    }
    else /* La lettre n'est pas presente dans l'arbre, fin de la recherche */
    {
      STOP = 1;
    }
  }
  /* printf("prec: %p\n", prec); */ /* TODO On est la */
  return prec;
}

arbre_t * creerNoeud(val_t valeur)
{
  arbre_t * nouv = NULL;

  if( (nouv = (arbre_t *) malloc(sizeof(arbre_t))) == NULL )
  {
    fprintf(stderr, "creerNoeud(): Echec allocation\n");
    exit(-1);
  }
  else /* Allocation reussie */
  {
    nouv->valeur  = valeur;
    nouv->fils    = NULL;
    nouv->frere   = NULL;
  }

  return nouv;
}


/* Cartouche fonction TODO */
void insertion(arbre_t ** r, val_t mot[])
{
  /* Parcourt les lettres deja existantes, ne peut jamais retourner NULL */
  /*puts("Nouvelle insertion");*/
  /*printf("r: %p\n", (void *) *r);*/
  arbre_t **  prec  = recherche(r, &mot);
  arbre_t *   nouv  = NULL;
  /*printf("prec: %p\n", (void *) *prec);*/

  while(*mot) /* Chainage des nouvelles lettres */
  {
      /*printf("creerNoeud( %c )\n", *mot);*/
      /* On cree un nouveau noeud et on passe a la lettre suivante */
      nouv          = creerNoeud(*mot++);
      nouv->frere   = *prec;
      *prec         = nouv;
      prec          = &((*prec)->fils); /* Passe au noeud suivant */
  }

  nouv->valeur =  toupper(nouv->valeur); /* On passe la derniere lettre du mot en majuscule */

  /*printf("Maj? %c\n",nouv->valeur);*/
}


/*******************************************************************************
 * La fonction génére un arbre à partir d'un fichier contenant une liste de mot
 *
 * @param nomFichier:  Nom du fichier a importer
 * @local mot : mot lu sur la ligne du fichier pour ensuite l'insérer dans un arbre
 * @local f : pointeur du fichier
 * @local n : Recupere le nombre de caracteres lus
 * @local r : pointeur de la racine de l'arbre

 * @return            Adresse de la racine de l'arbre
 ******************************************************************************/
arbre_t ** importerArbre(char * nomFichier)
{
  val_t       mot[50] = {0};
  FILE       *f       = fopen(nomFichier, "r");
  int         n       = 0;    /* Recupere le nombre de caracteres lus */
  arbre_t   **r       = NULL;

  if(f == NULL)
  {
    printf("Fichier inconnu\n");
  }
  else
  {
    printf(" \"%s\" ouvert\n", nomFichier);
    if( (r = (arbre_t **) malloc(sizeof(arbre_t *))) == NULL)
    {
      fprintf(stderr, "importerArbre(): Echec malloc racine 'r'\n");
    }
    else
    {
      * r = NULL; /* On initialise la racine de l'arbre a null */

      while( ! feof(f) )                           /* Lecture des lignes jusqu'a fin de fichier */
      {
        n = fscanf(f, "%s", (char *)mot);

        /* n = fscanf(f, " %100[^\n]", mot); */    /* Lecture jusqu'a '\n' */

        if(n != -1)                               /* On verifie qu'on a lu un message */
        {
          insertion(r, (val_t *)&mot);
        }
      }
      fclose(f);
    }
  }
  return r;
}
