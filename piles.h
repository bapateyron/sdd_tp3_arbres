/*******************************************************************************
*                               PILES.H
*
* Fichier en-tete qui declare les prototypes de toutes les fonctions
* gerant la manipulation des piles
*
*******************************************************************************/
#pragma once

  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include <ctype.h>
  #include "arbre.h"

  /*  Pour afficher la valeur de notre element empile, on specifie grace a ce
      define le type de destination pour caster */
  #define VALUE_CAST_TYPE  (char)
  #define VALUE_PRINTF "%c "
  #define VALUE_PROPRETY ->valeur

  /* Type generique a adapter en fonction du type des valeurs dans la pile */
  typedef arbre_t* item_t; /* Le type local item_t correspond au type arbre_t */

  /* Structure de la pile */
  typedef struct pile_t
  {
    int      taille;      /* Nombre d'elements */
    int      sommet;      /* Indice du dernier element ajoute */
    item_t  *  base;      /* Tableau des elements pointant sur le premier element ajoute */
  } pile_t;

  /* Prototypes *****************************************************************/
  pile_t  *  initialiserPile(int taille);
  int        estPileVide(pile_t * pile);
  void      empiler(pile_t * pile, item_t v, int * code);
  void      depiler(pile_t * pile, item_t * v, int * code);
  void      libererPile(pile_t * pile);
  void      afficherPile( pile_t * pile);

  /* Prototypes pour TP3 */
  item_t * listeElements( pile_t *pile, int * longueur);
