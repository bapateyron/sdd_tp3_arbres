FLAGS := -Wall -Wextra -ansi -pedantic -g

all: prog.bin

prog.o: main.c  arbre.c piles.c dictionnaire.c tests.c
	gcc $(FLAGS) -c main.c  arbre.c piles.c dictionnaire.c tests.c

prog.bin:  arbre.o piles.o dictionnaire.c tests.o main.o
	gcc $(FLAGS)  arbre.o piles.o dictionnaire.c tests.o  main.o -o prog.exe

clean:
	rm *.o *~ vgcore* *.exe
