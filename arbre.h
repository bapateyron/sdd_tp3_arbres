#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef char val_t;

typedef struct arbre_t {
  val_t              valeur;
  struct arbre_t  *  fils;
  struct arbre_t  *  frere;
} arbre_t;


arbre_t **  recherche(arbre_t ** r, val_t * mot[]);
arbre_t *   creerNoeud(val_t valeur);
void        insertion(arbre_t ** r, val_t mot[]);
arbre_t **  importerArbre(char nomFichier[]);
