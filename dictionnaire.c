/*******************************************************************************
Question 2

 Parcours l'arbre d'adresse a0 de facon a afficher les mots du dictionnaire
 dans l'ordre alphabetique

  @entree a0:      adresse de la racine de l'arbre qui est un pointeur de noeud

  @local pa   : pointeur de parcourt de l'arbre
  @local pile : pointeur de la pile
*******************************************************************************/

#include "dictionnaire.h"

void afficher_dico(arbre_t * r)
{

  arbre_t * pa      = r;                           /* pointeur de parcours */
  pile_t  * pile    = initialiserPile(1000);       /* pile qui stock une adresse et un entier */
  int       code    = 0;                           /* code d'erreur pour la pile */

  /*puts("Afficher Dico");*/

  while (pa != NULL)
  {

    /*puts("Afficher Mot");*/
    /*
    printf("%p\n", (void *)r);
    printf("%p\n", (void *)pa->fils);
    */


    /* TODO: recopier la méthode compacte */

    while (pa != NULL)          /*parcourt des fils*/
    {
      /*puts("While");*/
        empiler(pile, pa, &code);               /*on stock toutes les branches de l'arbre */
        /* printf("Lettre: %c\n", pa->valeur); */
        if (isupper(pa->valeur)) /*on regarde si la lettre est une majucule*/
        {
          afficherPile(pile);
          /* afficherMot(pile); */         /* Recupere la branche contenant le mot dans la pile */
        }
        pa = pa->fils;
    }

    /* printf("%p\n", (void *)pa); */

    while (pa != NULL && !estPileVide(pile))
    {
      depiler(pile, &pa, &code);
      pa = pa->frere;
    }
  }
  libererPile(pile);
}


void afficherDico(arbre_t * r)
{



  arbre_t * pa      = r;                           /* pointeur de parcours */
  pile_t  * pile    =NULL;       /* pile qui stock une adresse et un entier */
  int       code    = 0;                           /* code d'erreur pour la pile */
  /*puts("Afficher Dico");*/
  printf("Mot du Dico:\n" );
  if (r!=NULL)
  {
    pile    = initialiserPile(1000);
    while (!estPileVide(pile) || pa != NULL)
    {
      empiler(pile, pa, &code);               /*on stock toutes les branches de l'arbre */
      if (isupper(pa->valeur)) /*on regarde si la lettre est une majucule*/
      {
        /* afficherPile(pile); */ /* FONCTIONNE */
        afficherMot(pile);         /* Recupere la branche contenant le mot dans la pile */
      }
      /* printf("Lettre: %c\n", pa->valeur); */
      pa = pa->fils;

      /* printf("%p\n", (void *)pa); */

      while (pa == NULL && !estPileVide(pile))
      {
        depiler(pile, &pa, &code);
        pa = pa->frere;

      }
    }
    libererPile(pile);
  }
  else
  {
    printf("aucun mot\n" );
  }
  puts("");
}

/*******************************************************************************
 Cette fonction permet d'afficher le contenue des pointeur de la pile
 pour des adresses d'un arbre

  @entree     pile : pointeur de la pile
  @local  parcourt : pointeur de parcourt de l'arbre
  @local         i : Indice de parcours de la base de la pile
*******************************************************************************/
void afficherMot( pile_t * pile )
{

  int         i         = 0;  /* Indice de parcours de la pile */
  int         taille    = 0;  /* Taille de retour de listeElements */
  arbre_t **  elements  = (arbre_t **) listeElements(pile, &taille);

  for (i = 0; i < taille; i++) {
    printf("%c", tolower(elements[i]->valeur));
  }

  printf("\n");

  free(elements);
}

void libererArbre(arbre_t ** r)
{
  arbre_t * pa      = *r;                     /* pointeur de parcours */
  arbre_t * suivant = NULL;                   /* Pointeur temporaire pour free */
  pile_t  * pile    = initialiserPile(1000);  /* pile qui stock une adresse et un entier */
  int       code    = 0;                      /* code d'erreur pour la pile */
  while (!estPileVide(pile) || pa != NULL)
  {

    empiler(pile, pa, &code);   /* On stock toutes les noeuds de la branche actuelle */
    pa  = pa->fils;

    while (pa == NULL && !estPileVide(pile))
    {
      /*printf("pa2 =%p\n", *pa);*/
      depiler(pile, &pa, &code);  /* On depile les noeuds de la branche actuelle */
      suivant =  pa->frere;       /* Jusqu'a la prochaine branche (frere suivant) */
      free(pa);
      pa      = suivant;
    }
  }
  libererPile(pile);
  free(r);
  *r      = NULL;
  /*printf(" libérer\n");*/
}
