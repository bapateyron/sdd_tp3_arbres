/*******************************************************************************
*                               TESTS.C
*
* Implementation de toutes les fonctions de test qui pour chacune d'elles teste
* pour une fonction du programme en particulier, l'ensemble des cas inherents
* a cette fonction.
*
*******************************************************************************/

#include "tests.h"

char fichierCorrecte[] = "input.txt";



void test_recherche();
void test_insertion();

void test_importerArbre()
{
  arbre_t ** r  = importerArbre(fichierCorrecte);

  afficherDico(*r);
  libererArbre(r);
  /* Afficher arbre git */
}
