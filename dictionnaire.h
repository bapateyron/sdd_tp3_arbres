#pragma once

#include "arbre.h"
#include "piles.h"

void afficher_dico(arbre_t * r);
void afficherDico(arbre_t * r);
void afficherMot( pile_t * pile );
void libererArbre(arbre_t ** r);
