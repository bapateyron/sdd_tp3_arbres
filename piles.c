/*******************************************************************************
 *                                   PILES.C
 *
 * Implementation des fonctions du fichier piles.h qui permettent de realiser
 * les operations de manipulation elementaires des piles
 *
 ******************************************************************************/

 #include "piles.h"

/*******************************************************************************
 * Alloue puis initialise une pile de 'taille' elements, a condition que la
 * taille soit strictement positive
 *
 * @param   taille:  Taille totale du nombre de cellules pour la pile
 * @return           Adresse de la pile cree
 ******************************************************************************/
pile_t  *  initialiserPile(int taille)
{
  pile_t * pile = NULL;

  if(taille > 0)
  {
    pile  = (pile_t *) malloc(sizeof(pile_t));

    if(pile == NULL)
    {
      fprintf(stderr, "initialiserPile(): Allocation pile impossible");
    }
    else
    {
      pile->base    = (item_t *) malloc (sizeof(item_t) * taille);

      if(pile->base == NULL)
      {
        fprintf(stderr, "initialiserPile(): Allocation base impossible");
        free(pile);
        pile  = NULL;
      }
      else
      {
        pile->taille  = taille;
        pile->sommet  = -1;
      }
    }
  }
  else
  {
    printf("initialiserPile(): Une taille de pile (%d) est < 1 et n'est pas valide  -> Pile non cree\n", taille);
  }

  return pile;
}

/*******************************************************************************
 * Test si la pile est vide ou non (en utilisant l'indice de sommet de pile)
 *
 * @param    pile:  Pile a tester
 * @return         1 si la pile est vide, 0 sinon
 ******************************************************************************/
int   estPileVide(pile_t * pile)
{
  return pile->sommet == -1;
}

/*******************************************************************************
 * Insert une valeur 'v' en sommet de pile a condition que:
 *  - La pile existe
 *  - La pile ne soit pas pleine
 *
 * @param pile:  Adresse de la pile cible
 * @param  v:    Valeur a inserer dans la pile
 * @param  code:  Adresse pour renseigner un succes (0) ou un echec (1)
 ******************************************************************************/
void  empiler(pile_t * pile, item_t v, int * code)
{
  *code  = 1;  /* Le code passera a 0 (Succes) si on peut empiler */
  if(pile)
  {
    if (pile->sommet < (pile->taille -1))
    {
      pile->sommet++;
      pile->base[pile->sommet]  = v;
      * code                    = 0;
    }
    else
    {
      fprintf(stderr, "empiler(): Pile pleine, impossible d'empiler\n");
    }
  }
  else
  {
    fprintf(stderr, "empiler(): Pile inexistante, impossible d'empiler\n");
  }
}

/*******************************************************************************
 * Retire le sommet de pile et place sa valeur a l'adresse 'v' a condition que:
 *  - La pile existe
 *  - La pile ne soit pas pleine
 *
 * @param pile:  Adresse de la pile cible
 * @param  v:    Adresse dans laquelle placer la valeur depilee
 * @param  code:  Adresse pour renseigner un succes (0) ou un echec (1)
 ******************************************************************************/
void  depiler(pile_t * pile, item_t * v, int * code)
{
  *code  = 1;

  if (pile)
  {
    if ( !estPileVide(pile) )
    {
      *v    = pile->base[pile->sommet];
      pile->sommet--;
      *code  = 0;
    }
    else
    {
      fprintf(stderr, "depiler(): Pile vide, impossible de depiler");
    }
  }
  else
  {
    fprintf(stderr, "depiler(): Pile inexistante, impossible de depiler\n");
  }
}

/*******************************************************************************
 * Liberation memoire de la 'base' donc des elements de la pile
 * Puis dans un second temps, liberation de la structure de la pile elle-meme
 *
 * @param pile:  Adresse de la pile a liberer
 ******************************************************************************/
void  libererPile(pile_t * pile)
{
  if (pile != NULL)
  {
    if (pile->base != NULL)
    {
      free(pile->base);
    }

    free(pile);
  }
}

/*******************************************************************************
 * Affiche les elements de la pile (le debut de pile etant a gauche et le sommet
 * de pile etant la valeur la plus a droite) a condition que:
 *  - La pile existe
 *  - La pile ne soit pas vide
 *
 * @param pile:  Adresse de la pile a afficher
 ******************************************************************************/
void afficherPile( pile_t *pile)
{
  int i  = 0;  /* Indice de parcours de la pile */

  if (pile != NULL && pile->base != NULL )
  {
    if( !estPileVide(pile) )
    {
      printf("\n[ ");
      for (i = 0; i < pile->sommet + 1; i++)
      {
        printf(VALUE_PRINTF, VALUE_CAST_TYPE pile->base[i]VALUE_PROPRETY );
      }
      printf("]\n");
    }
    else
    {
      puts("\n[ Pile vide ]");
    }
  }
  else
  {
    fprintf(stderr, "afficherPile(): Pile non allouee\n");
  }
}

/*******************************************************************************
 * Retourne une copie des elements de la pile (le debut de pile etant a gauche
 * et le sommet de pile etant la valeur la plus a droite) a condition que:
 *  - La pile existe
 *  - La pile ne soit pas vide
 *
 * @param pile:  Adresse de la pile a afficher
 * @return      Tableau des elements contenus dans la pile
 ******************************************************************************/
item_t * listeElements(pile_t * pile, int * longueur)
{
  item_t * copie  = NULL; /* Tableau de copie de la pile */

  if (pile != NULL && !estPileVide(pile))
  {
    if( (copie = (item_t *) malloc(sizeof(item_t) * (pile->taille))) == NULL)
    {
      fprintf(stderr, "listeElements(): Allocation copie impossible");
      exit(-1);
    }
    else
    {
      for (*longueur = 0; *longueur < pile->sommet + 1; (*longueur)++)
      {
        copie[*longueur] = pile->base[*longueur];
      }
    }
  }
  else
  {
    fprintf(stderr, "listeElements(): Pile non allouee ou vide\n");
  }

  return copie;
}
